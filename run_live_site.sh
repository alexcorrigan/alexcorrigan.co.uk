#!/bin/bash

IMAGE_NAME="alexcorrigan-home-site"

docker build . -t $IMAGE_NAME

docker run -d --name $IMAGE_NAME --network net -e VIRTUAL_HOST="www.alexcorrigan.co.uk" -e LETSENCRYPT_HOST="www.alexcorrigan.co.uk" $IMAGE_NAME
