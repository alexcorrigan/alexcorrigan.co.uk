#!/bin/bash

IMAGE="registry.gitlab.com/alexcorrigan/alexcorrigan.co.uk:latest"
CONTAINER="alexcorrigan.co.uk"

echo "Stopping any running container..."
docker stop $CONTAINER
echo "Removing any existing container..."
docker container rm $CONTAINER
echo "Removing any existing image..."
docker image rm $IMAGE
echo "Pulling latest image..."
docker pull $IMAGE
echo "Running container..."
docker run -d --name $CONTAINER --network net -e VIRTUAL_HOST="www.alexcorrigan.co.uk" -e LETSENCRYPT_HOST="www.alexcorrigan.co.uk" $IMAGE
echo "=============="
echo "Site deployed!"
echo "=============="
