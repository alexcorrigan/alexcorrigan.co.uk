#!/bin/bash

DATE_TIME=`date +%Y%m%d-%H%M-`
TITLE=$1

LOCATION=posts/${DATE_TIME}${TITLE}.md

hugo new ${LOCATION}

$EDITOR content/${LOCATION}
