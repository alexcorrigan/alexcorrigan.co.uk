---

title: "Remote Docker Container Deployments from GitLab CI/CD Pipelines over SSH"
date: 2020-11-02T08:35:00Z
author: "Alex Corrigan"
categories: [professional]
tags: [blog development automation]
description: "How I deploy this site to a VPS... in a Docker container... from a GitLab CI/CD pipeline."

---

I've been running a VPS for a while now and its become a place where I try out and host a few little projects, one of which is this site. Everything that runs on there does so in its own container. I've only really seriously got into Docker over the past six months so am tending to run *everything* in a container right now. I know, I'm a bit late to the party, but it's awesome. How quickly I was able to spin up a seperate Grafana and Postgres container, connect them up and get analysing some data was a revelation to me (maybe more on this project in a later post).

One challenge I had with all these containers running on the same server was how to route traffic to them. Assigning each a different port to expose on a single domain wasn't going to be very useable for very long. After some searching I came across [Nginx Reverse Proxy](http://jasonwilder.com/blog/2014/03/25/automated-nginx-reverse-proxy-for-docker/) (by [Jason Wilder](http://jasonwilder.com/)), a service that runs in yet another container but will route inbound traffic to another based on the URL requested. When starting up a container, an environment varaible in the run command declares what it's virtual host is as well as the Docker network the Nginx reverse proxy is running on. You don't (usually) even need to expose any ports as Nginx Proxy can detect what is running in the container. Nginx Proxy therefore is the only service exposing any port to the outside world. Each site o app running on this network can then have its own domain or sub-domain. I found this guide [Using Docker to Set up Nginx Reverse Proxy With Auto SSL Generation](https://linuxhandbook.com/nginx-reverse-proxy-docker/) (at [Linux Handbook](https://linuxhandbook.com/)) helpful in setting this all up.

I recently switched to using [GitLab](https://about.gitlab.com/) for hosting the source code for this site (the project is public at [https://gitlab.com/alexcorrigan/alexcorrigan.co.uk](https://gitlab.com/alexcorrigan/alexcorrigan.co.uk)). This was mostly so I could make use of their CI/CD pipelines to have fully automated builds and deployments triggered on each commit to master. For the build stage of this pipeline I am running [Hugo](https://gohugo.io/) (the static site generator for this site) to generate the site atifacts that are then copied into an empty [Nginx Docker image](https://hub.docker.com/_/nginx). This gives me a nice compact image that is completlely self sufficient to serve up this website. The final step of the build stage is to then push this image to the container registry, another awesome feature of GitLab to have all of this in one project.

With the website image now availble to be pulled down to run on any server of my choosing, there was now a new challenge: how to get that image on to the VPS and run it as part of the pipeline. There is no way to run anything on the VPS itself to detect new commits or when the build stage has finisehd. 

Given each stage of the CI/CD pipeline runs in it's own Docker container, it seemed feasible that I'd be able to access the VPS from there and remotely execute a shell script to manage the Docker environment. There were a couple of steps to implement in order to achieve this:

#### Step 1: Get secure access to the server

This is all handled within the `.gitlab-ci.yml` pipeline configuration file, where we have the deploy stage described. All the set-up for SSH access to the VPS is within the `before_script` here:

```yaml {linenos=table,linenostart=22}
deploy:
    stage: deploy
    image: debian:buster
    before_script:
        - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )'
        - eval $(ssh-agent -s)
        - echo "$VPS_SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
        - mkdir -p ~/.ssh
        - chmod 700 ~/.ssh
        - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
        - chmod 644 ~/.ssh/known_hosts
    script:
        - ssh ${VPS_USER}@${VPS_HOST} < deploy_site.sh
    only:
        - master
```
([source](https://gitlab.com/alexcorrigan/alexcorrigan.co.uk/-/blob/master/.gitlab-ci.yml))

Using a plain Debian image, the first part ensures SSH is installed and starts a shell to which a private key is applied. This shell will be used for all subsequent SSH commands communicating with the VPS. The passphrase-less public/private key are set up prior to this with the public key held on the VPS, and the private key stored in a GitLab variable, `$VPS_SSH_PRIVATE_KEY`.

```
- 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )'
- eval $(ssh-agent -s)
- echo "$VPS_SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
```

The second part sets the known hosts for this image so it doesn't need to verify if the correct server is providing the publich key when the first connection is made:

```
- mkdir -p ~/.ssh
- chmod 700 ~/.ssh
- echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
- chmod 644 ~/.ssh/known_hosts
```

The value echo'd to the `know_hosts` file here is obtained by running the following on the VPS itself:

```
ssh-keyscan <domain name / IP address>
```

In my case, the domain name being `www.alexcorrigan.co.uk`. The full output returned from that command is stored in another GitLab variable, `$SSH_KNOWN_HOSTS`.

#### Step 2: The script to execute remotely

The script to deploy and run the docker image containing the website performs a number of other actions as well:

```bash {linenos=table}
#!/bin/bash

IMAGE="registry.gitlab.com/alexcorrigan/alexcorrigan.co.uk:latest"
CONTAINER="alexcorrigan.co.uk"

echo "Stopping any running container..."
docker stop $CONTAINER

echo "Removing any existing container..."
docker container rm $CONTAINER

echo "Removing any existing image..."
docker image rm $IMAGE

echo "Pulling latest image..."
docker pull $IMAGE

echo "Running container..."
docker run -d --name $CONTAINER --network net -e VIRTUAL_HOST="www.alexcorrigan.co.uk" -e LETSENCRYPT_HOST="www.alexcorrigan.co.uk" $IMAGE

echo "=============="
echo "Site deployed!"
echo "=============="
```
([source](https://gitlab.com/alexcorrigan/alexcorrigan.co.uk/-/blob/master/deploy_site.sh))

In sequence, this will ensure there are no containers running the website image, and any existing containers or images meeting this description are removed. It then pulls the latest image from the GitLab container registry before running it in a new container.

This is script is executed over SSH with the following line from `.gitlab-ci.yml`:

```
script:
	- ssh ${VPS_USER}@${VPS_HOST} < deploy_site.sh
```

Where I've set the user that owns the public/private keys and the IP address of the server as GitLab variables, `$VPS_USER` and `$VPS_HOST`.

The command to run command to run the container sets two environment variables required for this container to work with the Nginx Proxy. `VIRTUAL_HOST` sets what traffic will be directed to this container, identified by requests pointed at this URL. Alongside the Nginx Proxy service there is also a Let's Encrypt companion service that handles automatic creation, renewal and use of Let's Encrypt certificates. This provides certificates for each container run against the Nginx Proxy with the `LETSENCRYPT_HOST` variable set, and only for the domain / sub-domain provided there.

#### Conclusion

This was the final step in the development workflow I've been working on for some time. It's not occupied 100% of my time, but has been where I wanted to get for a while. To be able to work on content locally, push that to a git repo somewhere and then have some automated pipeline publish a new version of the website. Thankfully the majority of this can all be done within in one GitLab project. Though I am sure a similar set-up can be achieved with the likes of GitHub and Bitbucket, GitLab is just something I've got familiar with through work.

Here are the pipelines in action: [https://gitlab.com/alexcorrigan/alexcorrigan.co.uk/-/pipelines](https://gitlab.com/alexcorrigan/alexcorrigan.co.uk/-/pipelines)

I think the next thing to add to this will be some testing covering accessibility and validation etc.
