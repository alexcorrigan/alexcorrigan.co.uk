---
title: "This Week Under Test - Friday 16/10/2020"
author: Alex Corrigan
date: 2020-10-16T19:00:00+01:00
categories: ["professional"]
tags: ["testing", "this week under test"]
description: A collection interesting things I've read, found or used in my corner of the world of testing, and elsewhere, this past week.
---

I'll see if I can keep this up in the weeks to come, not least for the discipline of posting something regulary but also to try and keep my head up out of the water of my own little working bubble.

So here we go, five articles I've found some resonance with this past week...

#### [Ways of Positive Remote Working For a Tester](https://medium.com/code-wild/ways-of-positive-remote-working-for-a-tester-214e844f0d10)
I'm a tester and, like Jon here, our company found ourselves all working from home back in March this year. It's been a good motivator to read up on how others have met the challenge and even use it as an opportunity to try and improve their ways of working within their squads.

#### [Are there any limitations to test automation?](https://kevintuck.co.uk/are-there-any-limitations-to-test-automation/)
Some good points and elaboration on how automation is a tool that can aid and support us as testers our test activities. It's not the thing that defines or does our testing. A message I was trying to convey some time ago in "Automate the Small Things".

#### [How I Used Python and Selenium To Get a Lifetime Supply of Garlic Pizza Sticks](https://levelup.gitconnected.com/how-i-used-python-and-selen%20%20%20%20ium-to-get-a-lifetime-supply-of-garlic-pizza-sticks-94abb66b71e1)
Automation can be used for good or garlic pizza sticks. A leisurely walk-through detailing how some simple python and selenium automation can be used to exploit and cheat the system. But actually, putting garlic bread to one side, genuinely quite interesting regards to learning about some of the mechanics behind how websites like this work and what to look out for if we happen to be testing something like this one day.

#### [7 Things Awesome Testers do That Don’t Look Like Testing](https://sjpknight.com/post/7-things-awesome-testers-do-that-dont-look-like-testing/)
Nice all-round summary and promotional piece on all the skills testers employ and should be working that to some people might not look like actual testing.

#### [Twitter and Me](https://thefriendlytester.co.uk/2019/06/twitter-and-me)
Bit of an old one that I somehow stumbled across a few days ago. Discusses some of the things that have driven me away and drawn me back in to Twitter over the past few years. It's been great for finding out about new stuff I'm interested in, and following people I'm interested in. It's not so great for all the junk memes, politics and angst that pollute my timeline - occasionally retweets from the people I do want to follow but not for those reasons. I'm not sure if I'm ready to go back to it yet, but this article has got me tempted.
