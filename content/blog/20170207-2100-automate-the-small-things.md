---
title: Automate the Small Things
date: 2017-02-07T21:00:00+0100
author: Alex Corrigan
description: An approach to automation where automating tests is not necessarily the most rewarding thing you can spend your time on right now.
featured: true
categories: ["professional"]
tags: ["testing", "automation"]
---

I am certain this is not a general approach that applies in all situations. One project I was closely engaged with seemed to just want to spend money on automation to whatever end (they did, and it fizzled out to an undramatic end). This probably is not for them.

### Not to put you off automated tests, but...

I think automation should augment testers. Testers are smart and creative people; automation is dumb and can not think for itself. On day one of considering what to automate we should be thinking how best can we improve the effectiveness our test team and get them to be more efficient and engaged in interesting things. Interesting things also happen to be complex things where we do actually need real people to be engaged.

I feel there is too much attention paid to automating just the test execution. Automating tests is a big job, especially if there is no foundation in place to start with. Beyond that, the effort required to maintain them becomes an on-going battle. I have typically found projects set up with this goal take an initial investment of 6 months to get up and running (if they get up and running at all) with any sort of return on that, time savings, quick feedback on new builds etc, coming after a year from inception of the project.

Automating smaller things, however, can yield potentially greater benefits in the short term. Over time they will likely build up to a collection of automated processes that, when chained together, would form the foundation of a bespoke framework with which to automate those tests. The difference being that you will be getting value from automation now, not in 6 to 12 months.

### What are these small things?

The first place to look for automation candidates should be what testers are working on every day. They could be anything and not even necessarily related to test execution. These small things and might fall into one or more of the following categories:

#### Time Consuming
* Procedural tasks with well defined actions but take a long time. Whatever it is, if it takes an hour or longer then it's time consuming.

#### Repetitive
* When a thing is performed over and over again, a few times a day or every day.

#### Tedious
* Those things that have to be done but hold little intellectual merit, like copy/paste some evidence into a report.

#### Duplicated
* When an identical things is performed by more that one tester, either in parallel or at different times. This is a variation of a repetitive thing.

#### Error Prone
* Where the risk or impact of getting something wrong while doing the thing is high. Something like reorganising a folder structure where you might go an delete a load of critical files.

#### Painful
* Things that are "fiddly" or complicated, such as big, multi-part calculations, with logic.

These are all things you typically want to do manually only once, have the actions captured or recorded somehow, then move on. If they need to be performed again, then we want to just press a button or have it scheduled it will be done. More often than not they will be a combination of being repetitive and something else.

This is not your typical test automation and it is usually over looked. While the emphasis is not on automated tests, that is not to say any part of the test execution could not be included as a candidate. What we are looking to achieve here is to invest a little effort to build a small automated script, macro, recording, whatever, that when run and re-run saves time cumulatively. That time saved is time a tester can spend on more valuable and interesting work.

### An example

As a way to demonstrate what kind of value we get from this, take a typical task that a single tester runs on a daily basis. Something like preparing a set of reference data and loading it into a test environment.

    Time to perform task manually        ≈ 2 hours
    How many times a day                 = 1
    How many testers perform the task    = 1
    
    Effort to automate the task          ≈ 5 hours

While we have 1 test engineer building out the automation, everyone else is doing their job as usual. After 5 hours we have got a script that performs the task. It has been run a few times to test it and everyone has agreed it does what needs to be done. Great!

The next day the tester who usually performs the task manually runs the new automation script. The whole thing is completed in 3 minutes. They are stunned. That is 2 hours of their day they have just got back to do something more interesting.

Over one month (20 days)...

    Time saved                     = 20 x 2 hours
                                   = 40 hours

    After initial time to build    = 40 hours - 5 hours
                                   = 35 hours

Let us now assume that over the coming months we need to spend about 5 hours in each of those months to make some changes and keep the script up to date. So over the course of 6 months in total (20 days a month)...

    Time saved      = 6 months x 35 hours
                    = 210 hours
                    ≈ 26 days (8 hours / day)

In the time it takes to typically build out a test automation framework and automate a load of tests, by instead automating just one small task, we have won back, for one tester at least, just over a month of working hours. Imagine if we had also managed to automate a few other things!

You can spend that new found time however you like. A good use might be to gradually work on that test automation framework. Then over those first 6 months you have got automation working for you *and* you have made progress toward your goal of automating the regression test suite. Happy days <i class="em em-smile"></i>
