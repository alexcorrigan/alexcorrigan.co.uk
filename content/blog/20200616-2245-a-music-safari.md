---
title: "A Musical Safari: 16/06/2020"
date: 2020-06-16T22:45:00+0100
author: "Alex Corrigan"
description: "Starting from J.S. Bach's Toccata and Fugue in D Minor"
categories: ["personal"]
tags: [music,safari]

---

I do this every so often (quite often). I start listening to one bit of music and it makes me think of another, so then I listen to that. And that makes me think of another, so I listen to that... and so on. I thought I'd record the journey this time. The places I end up can be quite tangential or miles from where I began. It's fun, involves a bit of discovery and now I've documented it sort of justifies why I spent an hour doing it. So let's give this a go.

This evening I found myself humming something along the lines of Bach's Toccata and Fugure. Either that or something very similar that reminded me of it. And that is where we begin:

- J.S. Bach: Toccata and Fugure in D Minor ([youtube](https://www.youtube.com/watch?v=ho9rZjlsyYY))
- Jean Michel Jarre: Equinoxe Pt. 4 ([youtube](https://www.youtube.com/watch?v=iyX_qouAmfE))
- DJ Shadow: Organ Donor ([youtube](https://www.youtube.com/watch?v=U4E60Ffa9yQ))

This first batch of tracks definitely followed a bit of an 'organ' theme. But more than that, quite short and choppy organ playing. The ending on DJ Shadow with Organ Donor was from some distant memory when I listened to it a lot many years ago. And that provides the first transition in to something a little different, a bit of reuniting with some more of his tracks.

- DJ Shadow: Blood on the Motorway ([youtube](https://www.youtube.com/watch?v=_5OGk9N8Y5s))
- DJ Shadow: Six Days ([youtube](https://www.youtube.com/watch?v=eY-eyZuW_Uk))
- Groove Armada: Edge Hill ([youtube](https://www.youtube.com/watch?v=tkt4uwb_T2o))

Groove Armada I listened to about the same as DJ Shadow so that's why that track came up. Long, slow and atmospheric. I recall first hearing a section of it on some TV advert though can't remember which (it might have been one of the first trailers for Lost -- it wasn't, I checked).

This got me on to some movie soundtracks. Not sure why or what the connection was, but from here and for the next fair few tracks and artists there is a connection in the style and composition on them.

- John Murphy: Adagio in D Minor (from [Sunshine](https://www.imdb.com/title/tt0448134/?ref_=fn_al_tt_1)) ([youtube](https://www.youtube.com/watch?v=MGbC730C4BA))
- John Murphy: In the House in a Heartbeat (from [28 Days Later](https://www.imdb.com/title/tt0289043/?ref_=fn_al_tt_1) and [28 Weeks Later](https://www.imdb.com/title/tt0463854/?ref_=fn_al_tt_1)) ([youtube](https://www.youtube.com/watch?v=ST2H8FWDvEA))
- Murray Gold: Doomsday (from Dr. Who series 1 and 2) ([youtube](https://www.youtube.com/watch?v=e4Mtm5O6Qb4))
- Murray Gold: Abigail's Song (Silence is All You Know) (from Dr. Who, 2010 Christmas Special "A Christmas Carol") ([youtube](https://www.youtube.com/watch?v=Radrh60tAUk))
- Murray Gold: The Long Song (from Dr. Who series 7) ([youtube](https://www.youtube.com/watch?v=QV1RJk09vQ4))
- Clint Mansell: Lux Aterna (from [Requiem for a Dream](https://www.imdb.com/title/tt0180093/?ref_=fn_al_tt_1)) ([youtube](https://www.youtube.com/watch?v=CZMuDbaXbC8))
- Clint Mansell: The Last Man (from [The Fountain](https://www.imdb.com/title/tt0414993/?ref_=fn_al_tt_1)) ([youtube](https://www.youtube.com/watch?v=VUgC6215Gko))

I've always found these composers produce work of similar style and scale that I just like. I could listen to these tracks on repeat all day and just zone out, or in, and nothing would interupt me.

There then follows a soundtrack to Inception that also follows that same style of repeating and building musical phrases (is that the right term??).

- Hans Zimmer: Time (from [Inception](https://www.imdb.com/title/tt1375666/?ref_=fn_al_tt_1)) ([youtube](https://www.youtube.com/watch?v=RxabLA7UQ9k))
- Hans Zimmer: Chevaliers de Sangreal (from [The Da Vinci Code](https://www.imdb.com/title/tt0382625/?ref_=fn_al_tt_1)) ([youtube](https://www.youtube.com/watch?v=24WWwhgCLgM))

Then finally a couple of stragglers at the end...

- M83: Outro ([from [Melancholia](https://www.imdb.com/title/tt1527186/?ref_=fn_al_tt_1)) ([youtube](https://www.youtube.com/watch?v=Eyjj8BgsBGU))
- M83: Oblivion (from [Oblivion](https://www.imdb.com/title/tt1483013/?ref_=fn_al_tt_1)) ([youtube](https://www.youtube.com/watch?v=1mkUp1V3ys0))
- Ludovico Einaudi Experience ([youtube](https://www.youtube.com/watch?v=_VONMkKkdf4))
- Tyler Bates: Frank's Choice (from [The Punisher](https://www.imdb.com/title/tt5675620/?ref_=fn_al_tt_1)) ([youtube](https://www.youtube.com/watch?v=FoQteIvtOPA))

That last Tyler Bates track I found completely by accident by Youtube recommending me stuff based on what I had watched previously, but it totally fits with those same styles of John Murphy, Murray Gold and Clint Mansell from earlier. I had to include it.

That is where we end for this musical safari. Maybe I should do a Spotify or Youtube playlist or something, and then I can talk a bit more about each individual track rather than just list them out.

I am sure there will be more to follow....
