---
title: "This Week Under Test - Friday 30/10/2020"
author: Alex Corrigan
date: 2020-10-30T19:00:00+00:00
categories: ["professional"]
tags: ["testing", "this week under test"]
description: A collection interesting things I've read, found or used in my corner of the world of testing, and elsewhere, this past week.
---

Second installment here. Not quite weekly just yet. I'm building up to it let's say.

#### [Don't Let Flaky Tests Ruin Your Automation](https://dev-tester.com/dont-let-flaky-tests-ruin-your-automation/)
Tests can fail for any reason, and those failures could point to any number of problems with the application or even the infrasture on which it is running. They are a valuable asset and provide a safety net for commiting any amount of change to a product. When failures go unchecked or there is consent to ignore them within a team all that value goes, and worse, can lead to outright distrust in any of the tests. We need to keep them healthy or have a serious think about whether they are still worth having around... the tests that it is, not the team.

#### [Underscores in Java and Python Numbers](https://www.thetestingpirate.be/2020/10/21/underscores-in-java-and-python-numbers/)
I didn't know this was a thing at all. Maybe you didn't either.

#### [Falsehoods Programmers Believe About Time Zones](https://www.zainrizvi.io/blog/falsehoods-programmers-believe-about-time-zones/)
I don't think these are widely held falsehods, some probably not even commonly known at all. But given the recent move from BST to GMT in the UK (yes, I am one week behind on sharing this) it is an interesting read. It generates some sympathy for those who maintain all those date-time libraries we heavily rely upon in our code.

#### [This page is a truly naked, brutalist html quine.](https://secretgeek.github.io/html_wysiwyg/html.html)
An introduction to so many new terms: brutalism, naked objects, quines... and creative approaches to design of web pages and other things. Not entirely functional, but makes a change from the usual and something to play around with.

#### [Death to Bullshit](https://deathtobullshit.com/)
Amen to that. I have very little patience for sites that have so little respect for my attention.
