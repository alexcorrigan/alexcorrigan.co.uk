---
title: "A Cat Photo"
author: Alex Corrigan
date: 2021-01-11T00:33:00+01:00
categories: ["personal"]
tags: [pets]
description: Apache, our cat, in profile
---

Testing something with a camera, and so here is a cat!
![Apache](https://photos.smugmug.com/photos/i-rvrbm8V/0/a2f48ec4/L/i-rvrbm8V-L.jpg)
