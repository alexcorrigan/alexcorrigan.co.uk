---
title: UI Automation != Web Browser Automation
date: 2017-04-11T20:00:00+0100
author: Alex Corrigan
description: Please let's clarify what kind of UI Automation we are talking about.
categories: ["professional"]
tags: ["testing", "automation", "web", "gui", "tools" ]
---

There appears to be more accessibility to technologies, tools and services that offer or provide so called user interface or UI automation for testing. Or at least this has been my observation after coming across a lot of blog posts like these recently:

- [How *UI Automation* Testing Tools Are Important? Top 4 Tools Revealed](http://www.cygnet-infotech.com/blog/how-ui-automation-testing-tools-are-important-top-4-tools-revealed?utm_medium=social&utm_source=twitter.com&utm_campaign=Himani)
- [Using Lists to get *UI* Elements with Nearly Impossible Selectors](https://iamalittletester.wordpress.com/2017/04/10/using-lists-to-get-ui-elements-with-nearly-impossible-selectors/)
- [How I would approach creating automated *user interface*-driven tests](http://www.ontestautomation.com/how-i-would-approach-creating-automated-user-interface-driven-tests/)

One thing each of them, and others, has in common is that they are not really about taking user interface automation but rather web browser automation. There are a couple of open source technologies available that most of these articles reference (see [Selenium](http://www.seleniumhq.org/) and [Watir](https://watir.com/)). This is how we can tell the subjects are more specifically about web browser automation.

From each of the project's own web pages:

- Watir: "family of Ruby libraries for automating web browsers"
- Selenium: "Selenium automates browsers."

Perhaps even web browser automation is not even the correct description. They both really only provide the capability to automate interactions with web pages displayed in a web browser. What they can not do is store a bookmark in the browser or open the browser's developer console (just a quick, strange, diversion I found while checking that point: [Can We automate Bookmarking through Selenium](https://groups.google.com/forum/#!topic/selenium-users/svu8i6Sk_as)).

I realise I may have missed out on what appears to be an already accepted interpretation of "user interface" to mean web page or browser. But is "user interface", or even "graphical user interface" really the right description for this? Web pages, and interacting with or automating interactions with them via a web browser is an extreme specialisation. It is isolated from other user interface interactions such as typing on a keyboard or moving a mouse.
