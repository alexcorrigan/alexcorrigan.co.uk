---
title: "First Star Trail Photo Attempt"
date: 2020-06-08T22:59:55+0000
draft: false
author: "Alex Corrigan"
description: "Pointing my camera to the night sky"
categories: ["personal"]
tags: [astrophotography,photography,startrails,gimp]

---

I was inspired to get some photos of the ISS traveling across the night sky during the past month, but was a bit out of practice with my camera. So to get in training I attempted to capture a long exposure star trail photo. Capturing the stars (or rather the Earth) rotating about the pole.

The following photo was the result of about an hour long exposure, but in reality a composite of many shorter exposures.

- Camera = Canon EOS 7D
- Lens = Canon EF 17-40mm f/4L USM
- Focal Length = 23mm
- Aperture = f/4.5
- Exposure time = 14s
- ISO = 800
- No. Exposures ~ 200

![stars](https://photos.smugmug.com/Astrophotography/i-PbvQJx9/0/0b9552e0/L/startrail_20200529-L.jpg)

See the source photo [here](https://alexcorrigan.smugmug.com/Astrophotography/i-PbvQJx9/A)

I used Gimp to composite all the individual images, manually! It took ages. I will be looking for a more automated approach to this!
