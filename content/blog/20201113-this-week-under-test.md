---
title: "This Week Under Test - Friday 13/11/2020"
author: Alex Corrigan
date: 2020-11-13T19:00:00+00:00
categories: ["professional"]
tags: ["testing", "this week under test"]
description: A collection interesting things I've read, found or used in my corner of the world of testing, and elsewhere, this past week.
---

#### [The Testing Crab](https://changelog.com/posts/the-testing-pyramid-should-look-more-like-a-crab)
A kind of review on whether the good'ol [testing pyramid](https://martinfowler.com/articles/practical-test-pyramid.html#End-to-endTests) is still relevant and accurate today. I think, as with a lot of things, it's probably best used as a guide and aide to illustrate how much effort goes into the various types of test and how much / what type of value they provide. There is absolutely nothing wrong with our test loadouts representing a Crab(!) if that's what works for you.

#### Testing Machine Learning Systems
- [Effective testing for machine learning systems](https://www.jeremyjordan.me/testing-ml/)
- [ML Testing](https://synapse-qa.com/2020/11/10/machine-learning-and-quality-assurance/)

A couple of articles here on testing machine learning software and where comparisons can be made to testing paradigms with more functional systems. This has become a real focus for me recently in my own work and these two links have provided a good intro to starting to think about how to approach such testing.

#### [Industry 4.0](https://insights.sap.com/what-is-industry-4-0/)
I guess I kind of instinctively knew this was happening... but now I can put a name to it and start dropping it in to various conversations to look cool.

#### [Testing is "Easy"](https://thereluctanttester.com/2020/11/12/testing-is-easy/)
Yep.
