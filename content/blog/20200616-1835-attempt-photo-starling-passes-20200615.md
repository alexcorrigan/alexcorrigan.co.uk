---
title: "First Attempt to Capture Starlink Pass Overhead"
date: 2020-06-15T18:25:00+0100
author: "Alex Corrigan"
description: "Bit of a failure. Looking in wrong direction, not wide enough view, too much cloud... ?"
categories: ["personal"]
tags: [astrophotography,photography,starlink,satelites,startrails,gimp]

---

There was supposed to be, and I'm sure there was, a pass by of several of the Starlink satelite arrays at about 23:11 last night:

{{< tweet 1272240787087077376 >}}

I set my camera to take a load of exposures (details below), pointing roughly toward the southern skies. Unfortunatly, and it could have been for any number of reasons, I don't think I managed to capture a single one of the satelites. There wassome intermitant clound coverage, or I could have just been aiming too low / too high. I've not worked it out yet. I wasn't able to see any by eye either, so maybe just poor viewing conditions.

Anyway, this is what I got:

![startrails](https://photos.smugmug.com/Astrophotography/i-L9DMwbW/0/dd39e8d4/L/starlinkPass_20200614-L.jpg)

Source [here](https://alexcorrigan.smugmug.com/Astrophotography/i-L9DMwbW/A)

- Camera = Canon EOS 7D
- Lens = Canon EF 17-40mm f/4L USM
- Focal Length = 19mm
- Aperture = f/4.5
- Individual Exposure time = 14s
- Interval Between Exposures = 3s
- ISO = 640
- No. Exposures ~ 200
- Total Exposure ~59.7 minutes

Still haven't worked out a way to automate the image compositing in Gimp yet. I'm sure will when it becomes too painful a task to keep doing one image at a time.
