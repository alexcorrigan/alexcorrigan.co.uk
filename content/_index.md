---
title: "Home"
date: 2019-12-30T12:00:00Z
author: "Alex Corrigan"
---

<img src="/images/me.jpg" class="u-photo me-img img-responsive pull-left">

Greetings traveller. I'm [Alex Corrigan]({{< ref "/" >}}).

I'm a test engineer by trade but turn my hand to all things development and automation. I'm here for learning and sharing, and that's mostly why this site exists.

Below are a few of my most recent posts. My full [Blog]({{< ref "blog" >}}) has everytihng else.
