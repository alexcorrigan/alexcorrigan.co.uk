---
title: "Now"
type: static
---

_What am I up to these days..._

Working at [Origami](https://www.origamienergy.com/) as a Test Engineer

Living in [Cambridge, UK](https://osm.org/go/0EQHx--?m=)

<iframe width="212" height="175" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-11.71142578125%2C45.521743896993634%2C11.975097656250002%2C57.99645479967&amp;layer=mapnik&amp;marker=52.19413974159753%2C0.1318359375" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=52.194&amp;mlon=0.132#map=6/52.194/0.132">View Larger Map</a></small>
