---
title: "Hello Hugo"
date: 2019-12-29T12:56:27Z
author: "Alex Corrigan"
description: "Hugo basics"
categories: ["documentation"]
tags: ["hugo", "test"]
---

Hello there.

Learning me some Hugo...

start server and include drafts: `hugo server -D`

tags and categories can be defined in config.toml with taxonomies:

    [taxonomies]
        category = "categories"
        tag = "tags"

**Categories** bight be used for more broad grouping of posts eg. documentation

**Tags** might be used for more specific groupings eg. hugo, java, python

After modifying `config.toml`, restart the server.

Seems adding a new post that has a new category or tag (any taxonomy) requires a server restart aswell.
