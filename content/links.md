---
title: "Links"
type: static
---

Links? I got plenty of links:

### News and Feeds

- [BBC Tech News](https://www.bbc.com/news/technology)
- [Hacker News](https://news.ycombinator.com/)
- [Lobsters](https://lobste.rs/)
- [Ministry of Testing - Feeds](https://www.ministryoftesting.com/feeds/blogs) 
- [Software Testing Weekly](https://softwaretestingweekly.com)

### Blogs

#### People
- [Jeff Nyman (Stories from a Software Tester)](http://testerstories.com/)
- [Jamie Tanna](https://www.jvt.me/)
- [Yegor Bugayenko](https://www.yegor256.com/)
- [Kevin Tuck](https://kevintuck.co.uk/blog/)
- [Dennis Martinez (Dev Tester)](https://dev-tester.com/)
- [Andrew Knight (Automation Panda)](https://automationpanda.com/)
- [James Bach (Satisfice)](https://www.satisfice.com)
- [Richard Bradshaw (Friendly Tester)](https://thefriendlytester.co.uk)
- [Alan Page (Angry Weasel)](https://angryweasel.com)
- [Robert Day (Probe Testing)](https://probetesting700171536.wordpress.com)
- [Bug Hunter Sam](https://bughuntersam.com)
- [Michael Bolton (Develop Sense)](https://www.developsense.com/blog)
- [Lee Hawkins (Rocker Tester)](https://therockertester.wordpress.com)
- [Anne-Marie Charrett (Maverick Tester)](https://mavericktester.com/)
- [Maaret Pyhäjärvi (Visible Quality)](https://visible-quality.blogspot.com/)

#### Organisations
- [GOV.UK Technology Blog](https://technology.blog.gov.uk/)

### Katas

- [Project Euler](https://projecteuler.net/)
- [Codewars](https://www.codewars.com/)
- [Advent of Code](https://adventofcode.com/)
