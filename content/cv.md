---
title: "CV"
date: 2020-01-11T13:10:30Z
type: static
---

### Profile

I’m a Senior Test Engineer and Developer with over 15 years’ experience in quality assurance. As a champion of automation, I have built several full end-to-end test frameworks as well as various tools and utilities to improve the efficiency and effectiveness of the teams I have worked in.

I am reliable, pragmatic and a self-learner when faced with new domains, technologies or working practices. Where I encounter gaps in my knowledge or experience I enjoy the opportunity to expand my knowledge.

Whilst self-taught in most technologies used in development, I take responsibility to learn and implement best practices in the use of those technologies whether in small isolated projects or collaboratively in teams.

I have held long-term positions in greenfield investment banking and financial exchange projects and during those engagements have built up considerable business knowledge in a range of front to back office areas.

### Expertise

- Development and automation in an Agile / Test Driven Development environment
- Specification and build of bespoke tools for testing and automation
- Functional, System and Integration testing of both in-house developed and third-party applications
- Building good working and collaborative relationships with both development and business teams
- Skilled in traditional test processes and methodologies such as agile and waterfall as well as some varied hybrids, but also able to adapt to alternatives as required
- Excellent hands-on knowledge and experience in both testing and building common front to back-end technologies. For example:
    - Java, Python, JavaScript applications and services
    - Web servers and REST APIs
    - JMS, MQ Messaging protocols, as well as FIX and Swift in the finance industry
    - Message routing middleware
    - SQL and Mongo based databases
    - SOA and MicroService architectures
    - Linux command line and shell scripting

### Experience

#### Origami

Test Engineer

March 2020 – Present

#### Kinesis Money - New Gold and Silver-backed Cryptocurrency Exchange

Non-Functional Test Engineer

April 2019 – December 2019 (contract, 9 months)

- Plan and execute performance, disaster recover and failover testing of the core exchange platform Investigate, promote and help triage fixes for performance issues
- Build automation tools to facilitate functional testing by interfacing with exchange APIs and database
- Production support and incident management outside of UK business hours

#### Department for Work and Pensions - Universal Credit

Agile Test Consultant

September 2017 – September 2018 (contract, 12 months)

- Assuring acceptable automated test coverage of new features and performing further exploratory testing prior to feature sign off
- Building out and refining the automated test framework, improving efficiency of test runs where possible
- Working with product owners, business analysts and developers to define and improve team’s understanding of feature requirements, utilising product and technical experience
- Perform demonstrations and provide evidence of automated test coverage to product owners when signing off on new features

#### Certeco

Senior Consultant

August 2015 – August 2017

> #### Tullett Prebon - Implementing Electronic Market Surveillance
>
> Senior Test Analyst
>
> August 2015 – August 2017
> 
> - Devise strategy for automated testing of the market surveillance systems
> - Build and support automated test framework that would allow manual tests to be scripted and rerun on subsequent releases.
> - Advise test team of suitable and effective approaches to testing surveillance system alerting, reducing duplication and redundancy in overall test suite
> - Perform testing on the surveillance system’s interfaces to ensure correct mapping and interpretation of received messages to internal order and trade objects, and order and trade life-cycles.

#### BJSS

Technical Tester

October 2014 – August 2015

> #### Trafigura - Commodity Trade and Position Data Migration
> 
> Technical Tester
>
> October 2014 – August 2015
> 
> - Maintain and further develop automation framework that facilitated the migration and validation of trade and position data to upgraded management systems
> - Support business and development teams by investigating cause of position breaks post migration
> - Perform the production data migration on release weekends

#### Certeco

Senior Consultant

September 2013 – October 2014

> #### London Metal Exchange - Exchange / LMEclear Integration
> 
> Senior Test Analyst
>
> October 2013 – October 2014
> 
> - Ownership for the exchange side integration testing
> - Ensure LME’s existing exchange functionality with regards to clearing and trade reporting would not be impacted by the migration to another clearing house.
> - Specification and test execution of all trade interfaces between the exchange and new clearing systems
> - Devise and perform early FIX interface testing on the exchange systems to limit and dispose of high impact, blocking issues from being found during the main test execution phases
> - Conduct workshops with relevant business representatives on the more complex transactions between exchange and clearing systems to further clarify requirements
> - Low level root cause analysis of issues when scenarios and transactions between the exchange and clearing systems did not behave as expected
> 
> #### London Metal Exchange - EMIR Compliance
>
> Test Lead
>
> September 2013 – October 2013 (2 month project)
> 
> - Leading team of 5 test analysts
> - Defining scope of testing across the whole LME infrastructure as well as integration points with the London Clearing House
> - Planning, coordination and reporting of test execution progress

#### London Metal Exchange - Implementing Electronic Market Surveillance

Senior Test Analyst

November 2012 – August 2013 (contract, 9 months)

- Specification and execution of pattern detection and alerting tests within the surveillance system.
- Specification, execution and coordination of system integration testing between the surveillance system, exchange platforms and other source systems in the environment providing client, contract and price reference data.

#### NYSE Euronext - New CFD Trading Platform

Senior Test Analyst

April 2012 – August 2012 (contract, 4 months, project cancelled)

- Analyse requirements and define functional test scenarios to ensure the order management and smart order routing components were built to specification
- Integration testing with all other Tangent front and back-office components.
- Fast paced delivery where I was quickly able to draw on past experiences and knowledge in a similar environment and start contributing to many other areas aside from the core OMS.

#### HSBC GBM - Prime Services

Developer in Test
 
April 2010 – April 2012 (contract, 24 months)

- Providing agile development best practice and test automation expertise
- Specifying and implementing automated tests to ensure successful delivery of business acceptance criteria
- Working with both business analysts and developers to gain common and accepted understanding of the planned features
- Investigate issues exposed during daily continuous integration builds and production
- Handling trade capture, validation and persistence along with real time position building for reporting and risk / margin calculations

#### Trade Turquoise (now part of the London Stock Exchange Group)

QA Test Lead

March 2009 – March 2010

Initiate and execute test projects from planning to completion and implementation
Produce test plans to ensure the test strategy adhered to throughout projects as agreed with the project stakeholders
Reporting on the test progress and results provided by the team of QA Engineers
Test lead for TQ Lens (aggregated dark pools via smart order routing) successfully launched in October 2009
Test lead for Turquoise interoperability to multiple clearing houses
Test environment and release management

#### AppLabs (formerly IS Integration)

Senior Test Consultant

March 2007 – March 2009

> #### Trade Turquoise
> 
> Senior Test Engineer / Test Lead
> 
> February 2008 – March 2009
> 
> - Automating functional test cases for regression testing on each new code drop
> - Performance and stress testing the platform with a range trading profiles to mimic predicted and peak trading volumes within different time scales
> - Following the successful launch of Turquoise became test lead focused test projects ancillary to the core-trading platform
> - Running performance tests to bench mark and measure improvements to order entry FIX engines
> 
> #### Openwork
> 
> Senior Test Analyst
> 
> March 2007 – February 2008

#### Tesnet

Test Consultant

December 2004 – March 2007

> #### NTL (now Virgin Media)
> 
> Test Consultant December 2004 – March 2007
> 
> #### Avis Car Rentals
> 
> Automation Test Analyst May 2006 – July 2006

#### Comberton Village College

Web Administrator

September 2004 – December 2004

#### RoundYourWay.co.uk

Web Administrator (*1999 - 2000*)

### Certification

-  ISTQB Advanced Level – Test Manager 2014
-  PRINCE2 Foundation + Practitioner 2014
-  ISTQB Foundation Level 2007

### Education

- Coventry University 2000 – 2004 BA Graphic Design
- Hills Road Sixth Form College 1998 – 2000 A-Level Qualifications
- Bassingbourn Village College 1993 – 1998 GCSE Qualifications
