---
title: BASH
description: Stuff I've learnt working with BASH
---

### If

#### tests

if variable is empty / not empty
>`[ -z $VAR ] / [ ! -z $VAR ]`

if variable begins with aaaa
>`[[ $VAR == aaaa* ]]`

### Looping

#### while

read each line in a file
>`while read l; do echo $l; done < file.txt`

#### for

one liner, for each number in sequence
>`for i in {2..5}; do echo $i; done`

for files in `ls` query
>`#for i in *.ext; do echo $i; done`

### bash file / directory testing

eg:
>`if [ -f $BLA ]; then do this fi`

also these other test parameters:

>- `-b filename` - Block special file
>- `-c filename` - Special character file
>- `-d directoryname` - Check for directory Existence
>- `-e filename` - Check for file existence, regardless of type (node, directory, socket, etc.)
>- `-f filename` - Check for regular file existence not a directory
>- `-G filename` - Check if file exists and is owned by effective group ID
>- `-G filename set-group-id` - True if file exists and is set-group-id
>- `-k filename` - Sticky bit
>- `-L filename` - Symbolic link
>- `-O filename` - True if file exists and is owned by the effective user id
>- `-r filename` - Check if file is a readable
>- `-S filename` - Check if file is socket
>- `-s filename` - Check if file is nonzero size
>- `-u filename` - Check if file set-user-id bit is set
>- `-w filename` - Check if file is writable
>- `-x filename` - Check if file is executable

### directory

list directories only, from current location
>`ls -d */`

or pass each as argument:
>`for DIR in $(ls -d */); do echo $DIR; done`
