---
title: Kubernetes
description: Stuff I've learnt working with Kubernetes
---

# describe pod

`kubectl describe pod <pod name>`

# list namespaces for a cluster

`kubectl get namespace`

# forward local port to a port on a pod

`kubectl -na <namespace> port-forward <pod name> <local port>:<pod port>`

# kubernetes dashboard - runs dashboard on localhost port

`kubectl proxy`
