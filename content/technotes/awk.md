---
title: AWK
description: Stuff I've learnt working with AWK
---

print specific column of split string variable by custom delimiter

```bash
echo $VAR | awk -F ' :: ' '{print $2}'
```
